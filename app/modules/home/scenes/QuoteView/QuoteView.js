import React from 'react';
import {View, Text } from 'react-native';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import {Actions} from 'react-native-router-flux';
import {
    AdMobBanner,
    AdMobInterstitial,
    PublisherBanner,
    AdMobRewarded,
  } from 'react-native-admob';

import {connect} from 'react-redux';

import {actions as home} from "../../index"
const { getQuotes } = home;

import styles from "./styles"

class QuoteView extends React.Component {
    constructor() {
        super();
        this.state = {}
    }

    render() {
        console.log(this.props);
        const quote = this.props.quotes[this.props.index];
        if (!quote) {
            return (
                <View style={styles.container}>
                    <View style={styles.wrapper}>
                        <Text>Quote Not Found</Text>
                    </View>
                </View>
            );
        }

        return (
            <View style={styles.container}>
                <View style={{ ...styles.wrapper, backgroundColor: quote.color }}>
                    <Text style={styles.title}>{quote.text}</Text>
                </View>
                <AdMobBanner
                    adSize="fullBanner"
                    adUnitID="ca-app-pub-8613899973511420/3405330144"
                    testDevices={[AdMobBanner.simulatorId]}
                    onAdFailedToLoad={error => console.error(error)}
                />
            </View>
        )
    }
}

function mapStateToProps(state, props) {
    return {
        isLoading: state.homeReducer.isLoading,
        quotes: state.homeReducer.quotes
    }
}

export default connect(mapStateToProps, { getQuotes })(QuoteView);