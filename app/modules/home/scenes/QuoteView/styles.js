import {StyleSheet} from 'react-native';
import {theme} from "../../index";
const { normalize, padding, fontSize, fontFamily } = theme;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'column',
        justifyContent:"center",
        alignItems:"stretch",
        textAlign: 'center'
    },

    activityIndicator:{
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: "center"
    },
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
    },

    title: {
        fontSize:fontSize.large + 5,
        lineHeight:fontSize.large + 7,
        fontFamily: fontFamily.medium,
        color: "#fff",
        letterSpacing: 1
    },

    wrapper:{
        paddingHorizontal:15,
        paddingBottom: padding * 2,
        // height: '100%'
        // justifyContent:"center",
        // alignItems:"stretch",
        // textAlign: 'center'
    }
});

export default styles;